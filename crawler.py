import aiohttp
import asyncio
from bs4 import BeautifulSoup
from yarl import URL
import sys
from pathlib import Path
import aiofiles

bad_urls_file_path = '404s.txt'


async def fetch(client, url):
    async with client.get(url) as resp:
        if resp.status == 200:
            return True, await resp.text()
        else:
            return False, resp.status


def fix_url(url, base):
    if url.scheme not in {'', 'http', 'https'}:
        return None
    fixed = url
    if not url.is_absolute():
        fixed = base.join(fixed)
    if url.scheme == '':
        fixed = fixed.with_scheme('http')
    if url.is_absolute() and not (url.host == 'au.dk' or url.host.endswith('.au.dk')):
        return None
    if '.' in url.name:
        extension = Path(url.name).suffix
        if extension not in {'', 'html', 'htm', 'php'}:
            return None
    return fixed


def get_links(html_s, base_url):
    html = BeautifulSoup(html_s, 'lxml')
    links = set()
    new_base_elem = html.find('base')
    if new_base_elem:
        base_url = fix_url(URL(new_base_elem.get('href')), base_url)
        assert base_url is not None
    for a in html.find_all('a'):
        href = a.get('href')
        if href is None:
            continue
        url = URL(href)
        fixed_url = fix_url(url, base_url)
        if not fixed_url:
            continue
        links.add(fixed_url)
    return links


async def main():
    initial_url = URL('http://www.au.dk')
    queue = {(None, initial_url)}
    seen = set()
    bad_urls = []
    async with aiohttp.ClientSession() as client:
        async with aiofiles.open(bad_urls_file_path, 'w') as bad_urls_file:
            while queue:
                parent, curr_url = queue.pop()
                if curr_url in seen:
                    continue
                seen.add(curr_url)
                print(f'Q: {len(queue)}, seen: {len(seen)}, next: {curr_url}', end='\r')
                good, result = await fetch(client, curr_url)
                if good:
                    links = get_links(result, curr_url)
                    for l in links:
                        if l not in seen:
                            queue.add((curr_url, l))
                else:
                    status_code = result
                    if status_code == 404:
                        bad_urls.append((parent, curr_url))
                        print(f'404: {parent} -> {curr_url}', file=sys.stderr)
                    await bad_urls_file.write(f'{status_code}: {parent} -> {curr_url}\n')


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
